/*
   Name:            HNDAccountHandler.cls
   Requirement ID:
   Description:     Class to manage all the account related logic
                                         
  Version   | Author-Name                | Date         | Comment
============|============================|==============|===================== 
1.0         | Franco Vincent             | 30.11.2015   | initial version
1.1         | Franco                     | Comments added for demo


*/

public with sharing class HNDAccountHandler  
{
	public static void beforeInsert(list<Account> NewAccounts) 
	{
	// Set fields here if needed, instead of workflow logic.
	PreventDuplicateAccountName(NewAccounts);
	}
	public static void beforeUpdate(map<Id, Account> OldAccounts,map<Id, Account> NewAccounts) 
	{
	// Update fields here if needed, instead of workflow logic.
	PreventDuplicateAccountName(NewAccounts.Values());
	}
	
	public static void PreventDuplicateAccountName(list<Account> NewAccounts)
	{

		map<string,Account> AccountMap =  new map<string,Account>(); 
		if(NewAccounts.size() > 0)
		{
			for(Account objAccount : NewAccounts)
				AccountMap.put(objAccount.Name,objAccount);// Getting the key value pair with account name and account
				
			for(Account ExistingAccount :[SELECT Name FROM Account WHERE Name IN : AccountMap.KeySet()])
			{
				AccountMap.get(ExistingAccount.Name).addError('Account with this Name already exists.');
			}
		}
			
	}
}
/*
   Name:            AccountTrigger 
   Requirement ID:
   Description:     Trigger invoked when an account is created/Modified 
                                         
  Version   | Author-Name                | Date         | Comment
============|============================|==============|===================== 
1.0         | Franco Vincent             | 30.11.2015   | initial version
*/

trigger AccountTrigger on Account (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if (Trigger.isBefore) {
		if (Trigger.isInsert) {
			//Call class for before insert
			HNDAccountHandler.beforeInsert(Trigger.new);
		}
		if (Trigger.isUpdate) {
			//Call class for before update
			HNDAccountHandler.beforeUpdate(Trigger.oldMap,Trigger.newMap);
		}
	}
}